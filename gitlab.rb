#redis['enable'] = false

#gitlab_rails['redis_host'] = '172.19.0.2'
#gitlab_rails['redis_port'] = '6379'

postgresql['enable'] = false

postgresql['ssl'] = 'off'

gitlab_rails['db_adapter'] = 'postgresql'
gitlab_rails['db_encoding'] = 'utf8'
gitlab_rails['db_host'] = '172.19.0.3'
gitlab_rails['db_port'] = '5432'
gitlab_rails['db_username'] = 'c_hueser@web.de'
gitlab_rails['db_password'] = 'password'
gitlab_rails['db_database'] = "gitlab_ce_db"

external_url 'http://localhost:8080'
nginx['redirect_http_to_https'] = false
nginx['enable'] = false
nginx['listen_addresses'] = ['0.0.0.0', '[::]']
nginx['listen_port'] = 8080
nginx['listen_https'] = false
web_server['home'] = '/usr/share/nginx/html'
web_server['external_users'] = ['www-data']
gitlab_rails['trusted_proxies'] = [ '172.19.0.0/16' ]
#unicorn['enable'] = false
